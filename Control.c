/* Control Panel Test
 * Emergency stop -> toggle Alarm State
 *   Data LED 3 lit while emergency stop button pressed
 *
 * Pedestal sensor 1 reported  on interface LED 1 and LINK LED
 * Pedestal sensor 2 reported  on interface LED 2 and CONNECT LED
 */

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <leds.h>
#include <buttons.h>
#include <interface.h>
#include <control.h>
#include <can.h>
#include <lcd.h>

/*************************************************************************
*                   MESSAGE TYPES
*************************************************************************/
typedef enum messageDef {
Emergency_Stop = 1, /* Length: 0, Data: NULL */
Request_Pickup_R1 = 2, /* Length: 0, Data: NULL */
Request_Drop_R1 = 3, /* Length: 0, Data: NULL */
Piece_At_Conveyor = 4, /* Length: 0, Data: NULL */
Request_Pickup_R2 = 5, /* Length: 0, Data: NULL */
Error = 6, /* Length: 0, Data: NULL */
Pause = 7, /* Length: 0, Data: NULL */
Resume = 8, /* Length: 0, Data: NULL */
System_Run = 9 /* Length: 0, Data: NULL */
} messageDef;


/*************************************************************************
*                    FUNCTION DEFINITIONS
*************************************************************************/
void pauseSystem(void); // Pause system function
void unpauseSystem(void); // Unpause system function
void flashLeds(void); // Flash LEDs when paused function
void conveyorPickup(void); // Send message to pick up workpiece from conveyor
void startSystem(void); // Start system function
void stopEmergency(void); // Sends resume message to subsystems after emergency
/*************************************************************************
*                    OTHER DEFINITIONS
*************************************************************************/
canMessage_t txMsg; // Send message definition
canMessage_t rxMsg; // Receive message definition
bool systemInProgress = false; // Workpiece in system definition
bool systemOnline = false; // System online definition
 bool messageOk = false; // Message okay to send definition
  canMessage_t msg = {0, 0, 0, 0}; // Definition for sending message
  canMessage_t rxMsg = {0, 0, 0, 0}; // Definition for receiving message
  
uint8_t currentMessage = 0; // Current message display
bool pauseInProgress = false; // Boolean for pause in progress
bool systemPaused = false; // Boolean for system pause
bool systemEmergency = false; // Boolean for system emergency

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_MONITOR_SENS_PRIO = 4,
  APP_TASK_CTRL_PRIO
};

/*************************************************************************
*                     APPLICATION TASK STACKS
*************************************************************************/

enum {
  APP_TASK_MONITOR_SENS_STK_SIZE = 256,
  APP_TASK_CTRL_STK_SIZE = 256
};

static OS_STK appTaskMonitorSensStk[APP_TASK_MONITOR_SENS_STK_SIZE];
static OS_STK appTaskCtrlStk[APP_TASK_CTRL_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskMonitorSens(void *pdata);
static void appTaskCtrl(void *pdata);

/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  controlInit();
  lcdInit();
  buttonsInit();

  /* Initialise the OS */
  OSInit();                                                   

  /* Create Tasks */
  OSTaskCreate(appTaskMonitorSens,                               
               (void *)0,
               (OS_STK *)&appTaskMonitorSensStk[APP_TASK_MONITOR_SENS_STK_SIZE - 1],
               APP_TASK_MONITOR_SENS_PRIO);
   
  OSTaskCreate(appTaskCtrl,                               
               (void *)0,
               (OS_STK *)&appTaskCtrlStk[APP_TASK_CTRL_STK_SIZE - 1],
               APP_TASK_CTRL_PRIO);
   
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/

static void appTaskMonitorSens(void *pdata) {
  canMessage_t msg = {0, 0, 0, 0};
  bool messageOk = false;  
  
  /* Initialise the CAN message structure */
  msg.id = 0x07;  // arbitrary CAN message id
  msg.len = 4;    // data length 4
  msg.dataA = 0;
  msg.dataB = 0;
  
  /* Start the OS ticker
   * (must be done in the highest priority task)
   */
  osStartTick();
  
  /* 
   * Now execute the main task loop for this task
   */
  while (true) {
    interfaceLedSetState(D1_LED | D2_LED, LED_OFF);
    ledSetState(USB_LINK_LED, LED_OFF);
    ledSetState(USB_CONNECT_LED, LED_OFF);
    
    // Checks if item is present at Sensor 1 and whether system is online
    if (controlItemPresent(CONTROL_SENSOR_1) && systemOnline) { 
        interfaceLedSetState(D1_LED, LED_ON);
        ledSetState(USB_LINK_LED, LED_ON);
        systemInProgress = true;
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = Request_Pickup_R1;
          currentMessage = Request_Pickup_R1;
        }     
        
        OSTimeDly(3000);
       
       messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 3;
          currentMessage = 3;
        }
        
        OSTimeDly(3000);
        
         messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 4;
          currentMessage = 4;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 13;
          currentMessage = 13;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 14;
          currentMessage = 14;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 15;
          currentMessage = 15;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 16;
          currentMessage = 16;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 17;
          currentMessage = 17;
        }
        
        OSTimeDly(3000);
        
        messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 18;
          currentMessage = 18;
        }
        
        OSTimeDly(3000); 
        
    } 
    
    // Check if item is present at Sensor 2
    if (controlItemPresent(CONTROL_SENSOR_2) && systemInProgress) {
        interfaceLedSetState(D2_LED, LED_ON);
        ledSetState(USB_CONNECT_LED, LED_ON);
        

        systemInProgress = false;
    } 
    
    lcdSetTextPos(2,1);
    lcdWrite("DATA_A : %08x", currentMessage);
    OSTimeDly(20);
    
    while(systemInProgress)
    {
      interfaceLedSetState(D4_LED, LED_ON);
    }
    
  }
}

static void appTaskCtrl(void *pdata) {
    /* Initialise the CAN message structure */
  msg.id = 0x07;  // arbitrary CAN message id
  msg.len = 4;    // data length 4
  msg.dataA = 0;
  msg.dataB = 0;
  
  /* Initialise the CAN message structure */
  rxMsg.id = 0x07;  // arbitrary CAN message id
  rxMsg.len = 4;    // data length 4
  rxMsg.dataA = 0;
  rxMsg.dataB = 0;
  
  static bool emergency = false;
  interfaceLedSetState(D3_LED | D4_LED, LED_OFF);
  
  while (true) {
    
    
    // Pause function
   pauseSystem();
   
   // Unpause function
   unpauseSystem();
    
    // Flash LED while paused.
  flashLeds();
  
  // Start system function
  startSystem();
  
  // Stop emergency function
  stopEmergency();
    
    
    // Emergency button
    emergency = controlEmergencyStopButtonPressed();
    if (emergency) {
      controlAlarmToggleState();
      interfaceLedSetState(D4_LED, LED_ON);
      while (controlEmergencyStopButtonPressed()) {
        systemOnline = false;
        systemInProgress = false;
        systemEmergency = true;
        messageOk = canWrite(CAN_PORT_1,&msg);
        if(messageOk)
        {
          msg.dataA = Emergency_Stop;
        }
        OSTimeDly(20);
      }
    } else {
      interfaceLedSetState(D4_LED, LED_OFF);
      systemEmergency = false;
    }
    OSTimeDly(20);
  } 
  
}

/******************************************************************************
*pauseSystem()
*When BUT_1 is pressed, initiates Pausing of the system, waits until the block 
*reaches Sensor 2 then pauses the system.
******************************************************************************/
void pauseSystem()
{
// Pause function
    if(isButtonPressed(BUT_1))
    {
      pauseInProgress = true;
    }
      
      while(pauseInProgress)
      {
       if(systemInProgress == false)
       {
         messageOk = canWrite(CAN_PORT_1,&msg);
        if(messageOk)
        {
          msg.dataA = Pause;
        }
        OSTimeDly(100);
        systemPaused = true; 
        pauseInProgress = false;
       }
      }
     
}

/******************************************************************************
*unpauseSystem()
*Unpauses the system if already paused when BUT_1 is pressed.
******************************************************************************/
void unpauseSystem()
{
    if(isButtonPressed(BUT_1) && systemPaused)
    {
      messageOk = canWrite(CAN_PORT_1,&msg);
        if(messageOk)
        {
          msg.dataA = Resume;
        }
        OSTimeDly(100);
      systemPaused = false;
    }
}

/******************************************************************************
*flashLeds()
*Flashes Link LED when system is paused.
******************************************************************************/
void flashLeds()
{
while(systemPaused)
    {
      ledSetState(USB_LINK_LED, LED_ON);
      OSTimeDly(100);
      ledSetState(USB_LINK_LED, LED_OFF);
    }
}

/******************************************************************************
*conveyorPickup()
*Upon receiving a message from conveyor, sends a message to Robot 2 to pick up
*workpiece from conveyor.
******************************************************************************/
void conveyorPickup()
{
  if (canReady(CAN_PORT_1)) {  
      interfaceLedToggle(D1_LED);
      canRead(CAN_PORT_1, &rxMsg);
      
      if(rxMsg.dataA == 5)
      {
      messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 5;
          currentMessage = 5;
        }
      }
           
  }
}


/*****************************************************************************
*startSystem()
*Starts the system when BUT_2 is pressed, and sends a message via CAN
*****************************************************************************/

void startSystem()
{
  if(isButtonPressed(BUT_2) && !systemOnline)
  {
    systemOnline = true;
    messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = System_Run;
          currentMessage = System_Run;
        }
        interfaceLedSetState(D3_LED, LED_ON);
  }
  else if(isButtonPressed(BUT_2) && systemOnline)
  {
   if(!systemInProgress)
   {
     messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 19;
          currentMessage = 19;
        }
    systemOnline = false; 
    interfaceLedSetState(D3_LED, LED_OFF);
   }
  }
  OSTimeDly(100);
}

/******************************************************************************
*stopEmergency()
*Sends a message to stop system emergency when alarm is off
******************************************************************************/

void stopEmergency()
{
 if(isButtonPressed(JS_UP))
 {
   systemEmergency = false;
   if (messageOk) 
        {
          msg.dataA = Resume;
          currentMessage = Resume;
        }
 }
}