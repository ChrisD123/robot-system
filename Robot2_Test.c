/* Test Robot2
 *
 */
#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <can.h>

#define BUTTONS_TASK_ID 0
#define N_JOINTS 4

/**************************************************************************
*                       MESSAGE TYPES
**************************************************************************/
typedef enum {
Emergency_Stop = 1, /* Length: 0, Data: NULL */
Error = 6, /* Length: 0, Data: NULL */
Pause = 7, /* Length: 0, Data: NULL */
Resume = 8, /* Length: 0, Data: NULL */
Piece_Received = 9, /* Length: 0, Data: NULL */
R2_Initial_Pos = 10, /*Length: 0, Data: NULL */
R2_ReadyToPickup = 5, /*Length: 0, Data: NULL */
R2_MoveToInitial_Pos = 11,  /*Length: 0, Data: NULL */
} message_can;


/***************************************************************************
*                       PRIORITIES
***************************************************************************/

enum {
  APP_TASK_CAN_RECEIVE_PRIO = 4,
};

/****************************************************************************
*                  APPLICATION TASK STACKS
****************************************************************************/

enum { 
  APP_TASK_CAN_RECEIVE_STK_SIZE = 256
};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];

/*****************************************************************************
*                APPLICATION FUNCTION PROTOTYPES
*****************************************************************************/

static void appTaskCanReceive(void *pdata);

/*****************************************************************************
*                        GLOBAL FUNCTION DEFINITIONS
*****************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  interfaceInit(ROBOT);
  robotInit();


  /* Initialise the OS */
  OSInit();                                                   

  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);

  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/**************************************************************************
*                             APPLICATION TASK DEFINITIONS
****************************************************************************/


 static void appTaskCanReceive(void *pdata) {
  
  canMessage_t rxMsg;
  rxMsg.id = 0x07;  // arbitrary CAN message id
  rxMsg.len = 4;    // data length 4
  rxMsg.dataA = 0;
  rxMsg.dataB = 0;
  /* Start the OS ticker (highest priority task) */
  osStartTick();

  /* the main task loop for this task  */
  while (true) {
     // Try to receive message on CAN 1
    if (canReady(CAN_PORT_1)) {  
      canRead(CAN_PORT_1, &rxMsg);
      lcdSetTextPos(2,1);
      lcdWrite("ID     : %08x", rxMsg.id); 
      lcdSetTextPos(2,2);
      lcdWrite("LEN    : %08x", rxMsg.len); 
      lcdSetTextPos(2,3);
      lcdWrite("DATA_A : %08x", rxMsg.dataA); 
      lcdSetTextPos(2,4);
      lcdWrite("DATA_B : %08x", rxMsg.dataB); 
      lcdSetTextPos(2,5);
      lcdWrite("CAN1GSR: %08x", canStatus(1));
    }
    
    if (rxMsg.dataA == 5) {
      
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) < 60750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 5;
      }
      
    }
    /* Move Waist to the left */
    if (rxMsg.dataA == 5) {
           if(robotJointGetState((robotJoint_t)ROBOT_WAIST) < 89750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_WAIST, ROBOT_JOINT_POS_INC);
              robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_DEC);
           }
                 else {
          OSTimeDly(1000);
          rxMsg.dataA = 5;
      }
    }
    /* Wrist adjusted */
    if (rxMsg.dataA == 5) {
          if(robotJointGetState((robotJoint_t)ROBOT_WRIST) < 100000) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_WRIST, ROBOT_JOINT_POS_INC);
      }
      else {
          OSTimeDly(300);
          rxMsg.dataA = 5;
      }
    }
    /* Move to the lower position ready to pick up */
    if (rxMsg.dataA == 5) {
          if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) < 81750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 5;
      }
    }
    if (rxMsg.dataA == 5) {
          if(robotJointGetState((robotJoint_t)ROBOT_HAND) < 90750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_INC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 5;
      }
    }
    /* Move up */
    if (rxMsg.dataA == 5) {
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) > 60750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      else {
          OSTimeDly(1500);
          rxMsg.dataA = 5;
      }
    }
    if (rxMsg.dataA == 5) {
      if(robotJointGetState((robotJoint_t)ROBOT_WAIST) > 46750) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 5;
      }
    }
    /* Move to the lower position to drop object */
    if (rxMsg.dataA == 11) {
          if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) < 90000) {
              OSTimeDly(10);
               if(robotJointGetState((robotJoint_t)ROBOT_WRIST) < 50000) {
                  robotJointSetState((robotJoint_t)ROBOT_WRIST, ROBOT_JOINT_POS_INC);
               }
              robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 11;
      }
    }
    if (rxMsg.dataA == 11) {
      if (robotJointGetState((robotJoint_t)ROBOT_HAND) > 45000) {
              robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_DEC);
      }
      else {
          OSTimeDly(300);
         rxMsg.dataA = 11;
      }
    }
    if (rxMsg.dataA == 11) {
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) > 61000) {
              OSTimeDly(10);
              robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      else {
          OSTimeDly(1000);
          rxMsg.dataA = 18;
      }
    } 
    if (rxMsg.dataA == 18) {
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) > 61000) {
        robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_HAND) > 45000){
          robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WRIST) < 50000){
         robotJointSetState((robotJoint_t)ROBOT_WRIST, ROBOT_JOINT_POS_INC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WAIST) > 46750){
        robotJointSetState((robotJoint_t)ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
      }
      else {
        
          rxMsg.dataA = 7;
       
    }
      if (rxMsg.dataA == 7) {
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) > 61000) {
        robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_HAND) > 45000){
          robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WRIST) < 50000){
         robotJointSetState((robotJoint_t)ROBOT_WRIST, ROBOT_JOINT_POS_INC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WAIST) > 46750){
        robotJointSetState((robotJoint_t)ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
      }
      else {
         
          rxMsg.dataA = 18;
       
    }
      if (rxMsg.dataA == 6) {
      if(robotJointGetState((robotJoint_t)ROBOT_ELBOW) > 61000) {
        robotJointSetState((robotJoint_t)ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_HAND) > 45000){
          robotJointSetState((robotJoint_t)ROBOT_HAND, ROBOT_JOINT_POS_DEC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WRIST) < 50000){
         robotJointSetState((robotJoint_t)ROBOT_WRIST, ROBOT_JOINT_POS_INC);
      }
      if(robotJointGetState((robotJoint_t)ROBOT_WAIST) > 46750){
        robotJointSetState((robotJoint_t)ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
      }
      else {
        
          rxMsg.dataA = 18;
       
    }
    OSTimeDly(20);
 }
  
}
 }
 
  }
 }
