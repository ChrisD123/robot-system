/* Conveyor Test
 * JS UP -> forward
 *    DN -> reverse
 *    L,R -> stop
 * 
 * Conveyor sensor 1 reported  on interface LED 1 and LINK LED
 * Conveyor sensor 2 reported  on interface LED 2 and CONNECT LED
 *
 * item sensed at 1 and moving in reverse -> stop
 * item sensed at 2 and moving forward -> stop
 */

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <leds.h>
#include <buttons.h>
#include <interface.h>
#include <conveyor.h>

// set up booleans for the pause/resume and emergency stop states

bool systemInProgress = false;
bool systemPaused = false;
bool systemEmergency = false;

static uint8_t blocknum = 0;

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_MONITOR_SENS_PRIO = 4,
  APP_TASK_CTRL_CONV_PRIO
};

/*************************************************************************
*                  MESSAGES
*************************************************************************/

typedef enum messageDef {
Emergency_Stop = 1, /* Length: 0, Data: NULL */
Request_Pickup_R1 = 2, /* Length: 0, Data: NULL */
Request_Drop_R1 = 3, /* Length: 0, Data: NULL */
Piece_At_Conveyor = 4, /* Length: 0, Data: NULL */
Request_Pickup_R2 = 5, /* Length: 0, Data: NULL */
Error = 6, /* Length: 0, Data: NULL */
Pause_System = 7, /* Length: 0, Data: NULL */
Resume = 8, /* Length: 0, Data: NULL */
System_Run = 9 /* Length: 0, Data: NULL */
} messageDef;

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {
  APP_TASK_MONITOR_SENS_STK_SIZE = 256,
  APP_TASK_CTRL_CONV_STK_SIZE = 256
};

static OS_STK appTaskMonitorSensStk[APP_TASK_MONITOR_SENS_STK_SIZE];
static OS_STK appTaskCtrlConvStk[APP_TASK_CTRL_CONV_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskMonitorSens(void *pdata);
static void appTaskCtrlConv(void *pdata);

static void canHandler(void) {
  if (canReady(CAN_PORT_1)) {
    canRead(CAN_PORT_1, &can1RxBuf);
    OSSemPost(can1RxSem);
  }
}

/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  conveyorInit();
 
  /* Initialise the OS */
  OSInit();                                                   

  /* Create Tasks */
  OSTaskCreate(appTaskMonitorSens,                               
               (void *)0,
               (OS_STK *)&appTaskMonitorSensStk[APP_TASK_MONITOR_SENS_STK_SIZE - 1],
               APP_TASK_MONITOR_SENS_PRIO);
   
  OSTaskCreate(appTaskCtrlConv,                               
               (void *)0,
               (OS_STK *)&appTaskCtrlConvStk[APP_TASK_CTRL_CONV_STK_SIZE - 1],
               APP_TASK_CTRL_CONV_PRIO);
   
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/

static void appTaskMonitorSens(void *pdata) {
canMessage_t msg = {0, 0, 0, 0};
  bool messageOk = false; 
    
  /* Start the OS ticker
   * (must be done in the highest priority task)
   */
  osStartTick();
  
  /* 
   * Now execute the main task loop for this task
   */
  while (true) {
    interfaceLedSetState(D1_LED | D2_LED | D3_LED | D4_LED, LED_OFF);
    ledSetState(USB_LINK_LED, LED_OFF);
    ledSetState(USB_CONNECT_LED, LED_OFF);
    
    if (conveyorItemPresent(CONVEYOR_SENSOR_1)) {
        interfaceLedSetState(D1_LED, LED_ON);
        ledSetState(USB_LINK_LED, LED_ON);
    } 
    if (conveyorItemPresent(CONVEYOR_SENSOR_2)) {
        interfaceLedSetState(D2_LED, LED_ON);
        ledSetState(USB_CONNECT_LED, LED_ON);
    } 
    
    OSTimeDly(20);
  }
}

static void appTaskCtrlConv(void *pdata) {
  uint32_t btnState;
  
  while(true)
{

// if statements which will ensure that the sub-system can be initialised, paused and stopped in the event of an emergency
  
if (canReady(CAN_PORT_1)) {  
      canRead(CAN_PORT_1, &rxMsg);
	  
	  if(rxMsg.dataA == System_Running)
	  {
	  systemInProgress = true;
	  }
	  else 
	  if(rxMsg.dataA == Emergency_Stop)
	  {
	  systemEmergency = true;
	  conveyorSetState(CONVEYOR_OFF);
	  }
	  else if(rxMsg.dataA == Pause_System)
	  {
	  systemPaused = true;
	  }
}

// write the number of block in the system onto the LCD

lcdSetTextPos(2,1);
lcdWrite("Block number: %04x", blocknum);

	  // coveyor will only move if there is an object infront of sensor 1, but will check if there is an obejct in front of 
          // of sensor 2 before moving. it will also check that the conveyor isn'talready moving forward and that the states systemPaused
          // and systemEmergency have not been triggered
 if (conveyorItemPresent(CONVEYOR_SENSOR_1) &&
        !conveyorItemPresent(CONVEYOR_SENSOR_2) && 
          (conveyorGetState() != CONVEYOR_FORWARD && !systemPaused &&!systemEmergency)){
            /// increment number of blocks in the system
            blocknum++;
            conveyorSetState(CONVEYOR_FORWARD);
    }
	  
	  
}
	
        // stops the conveyor if there is an iten infront of sensor 2
	if (conveyorItemPresent(CONVEYOR_SENSOR_2) &&
        conveyorGetState() == CONVEYOR_FORWARD) {
      conveyorSetState(CONVEYOR_OFF);
      // decrement block count if block number if greater than 0
	blocknum--;
        if (blocknum > 0) {
        conveyorSetState(CONVEYOR_REVERSE);  
      
	  messageOk = canWrite(CAN_PORT_1,&msg);
        if (messageOk) 
        {
          msg.dataA = 1;
          
          {} 
    
    } 
    
    
        } } }
